package com.letsbuy.handler;


import com.letsbuy.domain.User;
import com.letsbuy.repositories.UserRepository;
import com.mongodb.DuplicateKeyException;
import com.mongodb.MongoWriteException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.function.Function;

@Service
public class UserHandler {

    @Autowired
    private UserRepository userRepository;

    private final ErrorHandler errorHandler=new ErrorHandler();


    @Autowired
    private PasswordEncoder passwordEncoder;

    public Mono<ServerResponse> findByUserName(ServerRequest request) {
        return userRepository.findByUserName(request.pathVariable("userName"))
                .flatMap(user -> ServerResponse.ok().body(Mono.just(user), User.class))
                .switchIfEmpty(ServerResponse.notFound().build());
    }

    public Mono<ServerResponse> saveUser(ServerRequest request) {

        Mono<User> savedUser  = request.bodyToMono(User.class).map(userObj -> {
            userObj.setPassword(passwordEncoder.encode(userObj.getPassword()));
            return userObj;
        });

        return savedUser.map(user -> this.userRepository.insert(user)).flatMap(u -> ServerResponse.ok().body(u,User.class))
                .onErrorResume(MongoWriteException.class,
                        t -> ServerResponse.status(HttpStatus.BAD_REQUEST).syncBody(t.getMessage()));

//       return this.userRepository.insert(savedUser).onErrorResume(
//               DuplicateKeyException.class,t-> ServerResponse.status(HttpStatus.INTERNAL_SERVER_ERROR).syncBody(t.getMessage()));

//        return ServerResponse.ok().body(this.userRepository.insert(user),User.class).onErrorMap(throwable -> throwable.printStackTrace());



    }

    public Mono<ServerResponse> findByUserId(ServerRequest request) {
        return this.userRepository.findById(request.pathVariable("id"))
                .flatMap(user -> ServerResponse.ok().body(Mono.just(user), User.class))
                .switchIfEmpty(ServerResponse.notFound().build());
    }

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder(11);
    }

}
