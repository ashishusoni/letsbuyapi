package com.letsbuy.handler;

public class InvalidParametersException extends Exception {

    public InvalidParametersException(final String message) {
        super(message);
    }
}