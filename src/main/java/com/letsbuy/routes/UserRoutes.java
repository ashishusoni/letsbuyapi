package com.letsbuy.routes;

import com.letsbuy.handler.UserHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.BodyInserters.fromObject;
import static org.springframework.web.reactive.function.server.RequestPredicates.GET;
import static org.springframework.web.reactive.function.server.RequestPredicates.POST;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;
import static org.springframework.web.reactive.function.server.RequestPredicates.accept;

@Configuration
public class UserRoutes {

    private final UserHandler userHandler;

    @Autowired
    public UserRoutes(UserHandler userHandler) {
        this.userHandler = userHandler;
    }

    @Bean
    public RouterFunction<?> routerFunction() {
        return
        route(GET("/api/user/{userName}").and(accept(MediaType.APPLICATION_JSON)), userHandler::findByUserName)
        .and(route(POST("/api/user/{id}").and(accept(MediaType.APPLICATION_JSON)), userHandler::findByUserId))
        .and(route(POST("/api/user").and(accept(MediaType.APPLICATION_JSON)), userHandler::saveUser));
}
}
