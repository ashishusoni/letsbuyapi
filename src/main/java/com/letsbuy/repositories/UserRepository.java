package com.letsbuy.repositories;

import com.letsbuy.domain.User;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;


public interface UserRepository  extends ReactiveMongoRepository<User,String> {
    public Mono<User> findByUserName(String userName);

//    @Override
//    <S extends User> Mono<S> insert(S entity);
}
